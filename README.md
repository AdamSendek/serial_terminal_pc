# Serial_Terminal_PC
Last update: 14-10-2018

## Description
Terminal to connect peripheliar modul to the port COM

## Software
Platform: Windows (C#)  
After start program, first press Refresch buttom to update port COM list.

## License
Creative Commons Attribution-NonCommercial-ShareAlike 4.0

## Photos
![](/ST_screan.png)
